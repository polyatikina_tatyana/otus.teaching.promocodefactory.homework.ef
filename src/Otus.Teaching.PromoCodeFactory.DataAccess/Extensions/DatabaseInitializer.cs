﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Common.Options;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;
using System.Threading.Tasks;


namespace Otus.Teaching.PromoCodeFactory.DataAccess.Extensions
{
    public class DatabaseInitializer
    {
        const string DB_INITIALIZE = "Инициализация базы данных";
        const string DATA_INITIALIZE = "Инициализация данных";
        
        private readonly DatabaseContext _databaseContext;
        private readonly ILogger<DatabaseInitializer> _logger;

        public DatabaseInitializer(DatabaseContext databaseContext,
            ILogger<DatabaseInitializer> logger)
        {
            _databaseContext = databaseContext;
            _logger = logger;
        }

        public Task SetInitializeAsync(DatabaseSettings _databaseSettings)
            => SetInitializeAsync(_databaseSettings?.RecreateDatabase ?? false, _databaseSettings?.InitializeFakeData ?? false);

        public async Task SetInitializeAsync(bool recreateDatabase, bool initializeFakeData)
        {
            _logger.LogInformation("{dbInitialize} - Старт", DB_INITIALIZE);

            await InitializeDatabaseStructAsync(recreateDatabase);

            if (initializeFakeData)
            {
                await AddFakeDataAsync();
            }

            _logger.LogInformation("{dbInitialize} - Стоп", DB_INITIALIZE);
        }

        private async Task InitializeDatabaseStructAsync(bool recreateDatabase)
        {
            if (recreateDatabase)
            {
                await _databaseContext.Database.EnsureDeletedAsync();

                _logger.LogWarning("{dbInitialize} - База данных удалена и будет пересоздана", DB_INITIALIZE);
            }

            await _databaseContext.Database.MigrateAsync();

            _logger.LogWarning("{dbInitialize} - Миграции выполнены успешно", DB_INITIALIZE);
        }

        private async Task AddFakeDataAsync()
        {
            _logger.LogWarning("{dataInitialize} - Старт", DATA_INITIALIZE);
            try
            {
                if (!await _databaseContext.Roles.AnyAsync())
                {
                    await _databaseContext.Roles.AddRangeAsync(FakeDataFactory.Roles);
                }
                
                if (!await _databaseContext.Employees.AnyAsync())
                {
                    await _databaseContext.Employees.AddRangeAsync(FakeDataFactory.Employees);
                }

                if (!await _databaseContext.Preferences.AnyAsync())
                {
                    await _databaseContext.Preferences.AddRangeAsync(FakeDataFactory.Preferences);
                }

                if (!await _databaseContext.Customers.AnyAsync())
                {
                    await _databaseContext.Customers.AddRangeAsync(FakeDataFactory.Customers);
                }

                await _databaseContext.SaveChangesAsync();

                _logger.LogWarning("{dataInitialize} - Выполнено успешно", DATA_INITIALIZE);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "{dataInitialize} - Ошибка", DATA_INITIALIZE);
            }

            _logger.LogWarning("{dataInitialize} - Стоп", DATA_INITIALIZE);
        }
    }
}
