﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Common.Exceptions;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        private readonly List<T> _data;

        public InMemoryRepository(IEnumerable<T> data)
        {
            _data = new List<T>(data);
        }

        public Task<IEnumerable<T>> GetAllAsync(
            params Expression<Func<T, object>>[] includeProperties)
            => Task.FromResult(_data.AsEnumerable());

        public Task<T> GetByIdAsync(Guid id,
            params Expression<Func<T, object>>[] includeProperties)
            => Task.FromResult(_data.FirstOrDefault(x => x.Id == id));

        public Task<IEnumerable<T>> GetByFilterAsync(Expression<Func<T, bool>> filter,
            params Expression<Func<T, object>>[] includeProperties)
            => Task.FromResult(_data.Where(filter.Compile()));

        public Task<bool> AnyAsync(Guid id)
            => Task.FromResult(_data.Any(x => x.Id == id));

        public Task<bool> AnyAsync(Expression<Func<T, bool>> filter)
            => Task.FromResult(_data.Any(filter.Compile()));

        public Task AddAsync(T entity)
        {
            if (_data.Any(x => x.Id == entity.Id))
            {
                ThrowConflictException();
            }

            _data.Add(entity);
            return Task.CompletedTask;
        }

        public Task AddRangeAsync(IEnumerable<T> entities)
        {
            var ids = entities.Select(e => e.Id).ToList();

            if (_data.Any(x => ids.Contains(x.Id)))
            {
                ThrowConflictException();
            }

            _data.AddRange(entities);
            return Task.CompletedTask;
        }

        public Task UpdateAsync(T entity)
        {
            if (!_data.Any(x => x.Id == entity.Id))
            {
                ThrowNotFountException();
            }

            var item = _data.FirstOrDefault(x => x.Id == entity.Id);
            var index = _data.IndexOf(item);

            _data.Remove(item);
            _data.Insert(index, entity);

            return Task.CompletedTask;
        }

        public Task RemoveAsync(Guid id)
        {
            if (!_data.Any(x => x.Id == id))
            {
                ThrowNotFountException();
            }

            _data.RemoveAll(x => x.Id == id);
            return Task.CompletedTask;
        }

        public Task RemoveAsync(Expression<Func<T, bool>> filter)
        {
            if (!_data.All(x => filter.Compile().Invoke(x)))
            {
                ThrowNotFountException();
            }

            _data.RemoveAll(x => filter.Compile().Invoke(x));
            return Task.CompletedTask;
        }

        private void ThrowNotFountException() => throw new NotFoundException();
        private void ThrowConflictException() => throw new ConflictException();
    }
}