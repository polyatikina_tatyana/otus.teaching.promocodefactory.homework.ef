﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions options)
            : base(options)
        {
        }

        public DatabaseContext() { }

        public DbSet<Employee> Employees { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<Customer> Customers { get; set; }

        public DbSet<Preference> Preferences { get; set; }

        public DbSet<PromoCode> PromoCodes { get; set; }

        public DbSet<CustomerPreference> CustomerPreferences { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
                        
            modelBuilder.Entity<Role>()
                .HasIndex(i => i.Name)
                .IsUnique();

            modelBuilder.Entity<Employee>()
                .HasOne(u => u.Role)
                .WithMany(c => c.Employees)
                .HasForeignKey(u => u.RoleId);

            modelBuilder.Entity<Customer>();

            modelBuilder.Entity<Preference>(entity =>
                {
                    entity.HasIndex(i => i.Name).IsUnique();

                    entity.HasMany(u => u.PromoCodes)
                    .WithOne(c => c.Preference)
                    .HasForeignKey(c => c.PreferenceId);
                });

            modelBuilder.Entity<PromoCode>(entity =>
            {
                entity.HasIndex(i => i.Code).IsUnique();

                entity.HasOne(u => u.Customer)
                .WithMany(c => c.PromoCodes)
                .HasForeignKey(u => u.CustomerId);

                entity.HasOne(u => u.PartnerManager)
                .WithMany(c => c.PromoCodes)
                .HasForeignKey(u => u.PartnerManagerId);

            });

            modelBuilder.Entity<CustomerPreference>(entity =>
            {
                entity.HasIndex(i => new { i.CustomerId, i.PreferenceId },
                    "UX_customer_reference_customer_id_preference_id")
                .IsUnique();

                entity.HasOne(u => u.Customer)
                .WithMany(c => c.CustomerPreferences)
                .HasForeignKey(u => u.CustomerId);

                entity.HasOne(u => u.Preference)
                .WithMany(c => c.CustomerPreferences)
                .HasForeignKey(u => u.PreferenceId);
            });
        }
    }
}
