﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.Responses
{
    /// <summary>
    /// Сотрудник
    /// </summary>
    public class EmployeeResponse : EmployeeShortModel
    {
        /// <summary>
        /// Роль
        /// </summary>
        public RoleModel Role { get; set; }
    }
}
