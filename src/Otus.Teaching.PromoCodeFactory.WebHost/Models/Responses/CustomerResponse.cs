﻿using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.Responses
{
    /// <summary>
    /// Клиент
    /// </summary>
    public class CustomerResponse : CustomerShortModel
    {
        /// <summary>
        /// Промо-коды
        /// </summary>
        public List<PromoCodeShortModel> PromoCodes { get; set; }

        /// <summary>
        /// Предпочтения
        /// </summary>
        public List<PreferenceModel> Preferences { get; set; }
    }
}