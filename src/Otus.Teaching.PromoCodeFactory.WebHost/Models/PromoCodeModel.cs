﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    /// <summary>
    /// Промо-код
    /// </summary>
    public class PromoCodeModel : PromoCodeShortModel
    {       

        /// <summary>
        /// Менеджер партнера
        /// </summary>
        public EmployeeShortModel PartnerManager { get; set; }

        /// <summary>
        /// Льгота
        /// </summary>
        public PreferenceModel Preference { get; set; }

        /// <summary>
        /// Клиент
        /// </summary>
        public CustomerShortModel Customer { get; set; }
    }
}
