﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    /// <summary>
    /// Промо-код
    /// </summary>
    public class PromoCodeShortModel
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Код
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Сервисная информация
        /// </summary>
        public string ServiceInfo { get; set; }

        /// <summary>
        /// Период действия - С
        /// </summary>
        public string BeginDate { get; set; }

        /// <summary>
        /// Период действия - ПО
        /// </summary>
        public string EndDate { get; set; }

        /// <summary>
        /// Партнер
        /// </summary>
        public string PartnerName { get; set; }
    }
}