﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    /// <summary>
    /// Сотрудник
    /// </summary>
    public class EmployeeShortModel
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// Клиент
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Электронная почта
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Кол-во использованных промо-кодов
        /// </summary>
        public int AppliedPromocodesCount { get; set; }
    }
}