﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class CustomerShortModel
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// Клиент
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Электронная почта
        /// </summary>
        public string Email { get; set; }
    }
}