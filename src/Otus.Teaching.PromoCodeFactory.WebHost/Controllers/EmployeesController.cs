﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Managers;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Responses;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private const string CONTROLLER_NAME = nameof(EmployeesController);

        private ILogger<CustomersController> _logger;
        private readonly IUnitOfWork _unitOfWork;

        public EmployeesController(IUnitOfWork unitOfWork, ILogger<CustomersController> logger)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortModel>> GetEmployeeShortInfosAsync()
        {
            var employees = await _unitOfWork.GetRepository<Employee>().GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortModel()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet("getAll")]
        public async Task<List<EmployeeResponse>> GetEmployeesAsync()
        {
            var employees = await _unitOfWork.GetRepository<Employee>().GetAllAsync(e => e.PromoCodes, e => e.Role);

            var employeesModelList = employees.Select(employee =>
                new EmployeeResponse()
                {
                    Id = employee.Id,
                    Email = employee.Email,
                    Role = new RoleModel()
                    {
                        Id = employee.Role.Id,
                        Name = employee.Role.Name,
                        Description = employee.Role.Description
                    },
                    FullName = employee.FullName,
                    AppliedPromocodesCount = employee.AppliedPromocodesCount
                }).ToList();

            return employeesModelList;
        }

        /// <summary>
        /// Получить данные сотрудника по идентификатору
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeModel>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _unitOfWork.GetRepository<Employee>().GetByIdAsync(id, e => e.PromoCodes, e => e.Role);

            if (employee == null)
                return NotFound();

            var employeeModel = new EmployeeModel()
            {
                Id = employee.Id,
                Email = employee.Email,
                Role = new RoleModel()
                {
                    Id = employee.Role.Id,
                    Name = employee.Role.Name,
                    Description = employee.Role.Description
                },
                FirstName = employee.FirstName,
                LastName = employee.LastName,
                MiddleName = employee.MiddleName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Добавить сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPost("add")]
        public async Task<ActionResult> AddAsync([FromBody] EmployeeModel employee)
        {
            if (await _unitOfWork.GetRepository<Employee>().AnyAsync(employee.Id))
            {
                return Conflict();
            }

            try
            {
                await SaveRoleAsync(employee.Role);
                await _unitOfWork.GetRepository<Employee>().AddAsync(new Employee()
                {
                    Id = employee.Id,
                    Email = employee.Email,
                    RoleId = employee.Role.Id,
                    FirstName = employee.FirstName,
                    LastName = employee.LastName,
                    MiddleName = employee.MiddleName,
                    AppliedPromocodesCount = employee.AppliedPromocodesCount
                });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.InnerException ?? ex, "{CONTROLLER_NAME} - Ошибка при выполнении метода", CONTROLLER_NAME);
                return BadRequest();
            }

            return Ok();
        }

        /// <summary>
        /// Изменить сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPut("update")]
        public async Task<ActionResult> UpdateAsync([FromBody] EmployeeModel employee)
        {
            var existEmployee = await _unitOfWork.GetRepository<Employee>().GetByIdAsync(employee.Id);

            if (existEmployee == null)
            {
                return NotFound();
            }

            try
            {
                using var transaction = await _unitOfWork.BeginTransactionAsync();
                try
                {
                    await SaveRoleAsync(employee.Role);

                    existEmployee.RoleId = employee.Role.Id;
                    existEmployee.Email = employee.Email;
                    existEmployee.LastName = employee.LastName;
                    existEmployee.FirstName = employee.FirstName;
                    existEmployee.MiddleName = employee.MiddleName;
                    existEmployee.AppliedPromocodesCount = employee.AppliedPromocodesCount;

                    await _unitOfWork.GetRepository<Employee>().UpdateAsync(existEmployee);

                    await transaction.CommitAsync();
                }
                catch
                {
                    await transaction.RollbackAsync();
                    throw;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.InnerException ?? ex, "{CONTROLLER_NAME} - Ошибка при выполнении метода", CONTROLLER_NAME);
                return BadRequest();
            }

            return Ok();
        }

        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteAsync(Guid id)
        {
            if (!await _unitOfWork.GetRepository<Employee>().AnyAsync(id))
            {
                return NotFound();
            }

            try
            {
                await _unitOfWork.GetRepository<Employee>().RemoveAsync(id);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.InnerException ?? ex, "{CONTROLLER_NAME} - Ошибка при выполнении метода", CONTROLLER_NAME);
                return BadRequest();
            }

            return Ok();
        }

        private async Task SaveRoleAsync(RoleModel role)
        {
            if (role == null)
            {
                return;
            }

            var existRole = await _unitOfWork.GetRepository<Role>().GetByIdAsync(role.Id);
            if (existRole == null)
            {
                await _unitOfWork.GetRepository<Role>().AddAsync(new Role
                {
                    Id = role.Id,
                    Name = role.Name,
                    Description = role.Description
                });
            }
            else
            {
                existRole.Description = role.Description;
                existRole.Name = role.Name;

                await _unitOfWork.GetRepository<Role>().UpdateAsync(existRole);
            }
        }
    }
}