﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Роли сотрудников
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class RolesController
        : ControllerBase
    {
        private const string CONTROLLER_NAME = nameof(RolesController);

        private ILogger<CustomersController> _logger;
        private readonly IRepository<Role> _rolesRepository;

        public RolesController(IRepository<Role> rolesRepository, ILogger<CustomersController> logger)
        {
            _rolesRepository = rolesRepository;
            _logger = logger;
        }

        /// <summary>
        /// Получить все доступные роли сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<RoleModel>> GetRolesAsync()
        {
            var roles = await _rolesRepository.GetAllAsync();

            var rolesModelList = roles.Select(x => 
                new RoleModel()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description
                }).ToList();

            return rolesModelList;
        }

        /// <summary>
        /// Получить роль по идентификатору
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<RoleModel>> GetRoleByIdAsync(Guid id)
        {            
            var role = await _rolesRepository.GetByIdAsync(id);
            if (role == null) return NotFound();

            return new RoleModel()
            {
                Id = role.Id,
                Name = role.Name,
                Description = role.Description
            };
        }

        /// <summary>
        /// Добавить роль
        /// </summary>
        /// <returns></returns>
        [HttpPost("add")]
        public async Task<ActionResult> AddAsync([FromBody] RoleModel role)
        {
            if (await _rolesRepository.AnyAsync(role.Id))
            {
                return Conflict();
            }

            try
            {
                await _rolesRepository.AddAsync(new Role
                {
                    Id = role.Id,
                    Name = role.Name,
                    Description = role.Description
                });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.InnerException ?? ex, "{CONTROLLER_NAME} - Ошибка при выполнении метода", CONTROLLER_NAME);
                return BadRequest();
            }

            return Ok();
        }

        /// <summary>
        /// Изменить роль
        /// </summary>
        /// <returns></returns>
        [HttpPut("update")]
        public async Task<ActionResult> UpdateAsync([FromBody] RoleModel role)
        {
            var existRole = await _rolesRepository.GetByIdAsync(role.Id);
            if (existRole == null) return NotFound();

            try
            {
                existRole.Description = role.Description;
                existRole.Name = role.Name;

                await _rolesRepository.UpdateAsync(existRole);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.InnerException ?? ex, "{CONTROLLER_NAME} - Ошибка при выполнении метода", CONTROLLER_NAME);
                return BadRequest();
            }

            return Ok();
        }

        /// <summary>
        /// Удалить роль
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteAsync(Guid id)
        {
            if (!await _rolesRepository.AnyAsync(id))
            {
                return NotFound();
            }

            try
            {
                await _rolesRepository.RemoveAsync(id);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.InnerException ?? ex, "{CONTROLLER_NAME} - Ошибка при выполнении метода", CONTROLLER_NAME);
                return BadRequest();
            }

            return Ok();
        }
    }
}