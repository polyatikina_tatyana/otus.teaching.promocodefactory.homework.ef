﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Managers;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Requests;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промо-коды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromoCodesController
        : ControllerBase
    {
        private const string CONTROLLER_NAME = nameof(PromoCodesController);

        private ILogger<CustomersController> _logger;
        private readonly IUnitOfWork _unitOfWork;

        public PromoCodesController(IUnitOfWork unitOfWork, ILogger<CustomersController> logger)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
        }

        /// <summary>
        /// Получить все промо-коды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortModel>>> GetPromoCodesAsync()
        {
            var list = await _unitOfWork.GetRepository<PromoCode>().GetAllAsync();

            return Ok(list.Select(x => new PromoCodeShortModel
            {
                Id = x.Id,
                BeginDate = x.BeginDate.ToString("dd.MM.yyyy HH:mm:ss zzz"),
                EndDate = x.EndDate.ToString("dd.MM.yyyy HH:mm:ss zzz"),
                Code = x.Code,
                PartnerName = x.PartnerName,
                ServiceInfo = x.ServiceInfo
            }).ToList());
        }
        
        /// <summary>
        /// Создать промо-код и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodesRequest request)
        {
            var preference = (await _unitOfWork.GetRepository<Preference>().GetByFilterAsync(x => x.Name.Equals(request.Preference)))?.FirstOrDefault();
            if (preference == null)
            {
                return BadRequest();
            }

            try
            {
                var promoCodes = request.PromoCodes.Select(promoCode => new PromoCode
                {
                    Id = Guid.NewGuid(),
                    BeginDate = DateTimeOffset.UtcNow,
                    EndDate = DateTimeOffset.UtcNow.AddDays(100),
                    Code = promoCode,
                    PartnerName = request.PartnerName,
                    ServiceInfo = request.ServiceInfo,
                    PreferenceId = preference.Id,
                    PartnerManagerId = request.PartnerManagerId
                });

                var customers = await _unitOfWork.GetRepository<Customer>()
                                                 .GetByFilterAsync(x => x.CustomerPreferences.Any(p => p.PreferenceId == preference.Id)
                                                                 , c => c.PromoCodes);
                var queue = new Queue<Customer>();

                customers
                    .GroupBy(x => x.PromoCodes.DefaultIfEmpty().Max(x => x?.Modify ?? DateTime.MinValue))
                    .OrderBy(x => x.Key)
                    .SelectMany(x => x.ToList())
                    .ToList()
                    .ForEach(queue.Enqueue);

                using var transaction = await _unitOfWork.BeginTransactionAsync();
                try
                {
                    foreach (var promoCode in promoCodes)
                    {
                        var customerQueue = queue.Peek();

                        promoCode.CustomerId = customerQueue.Id;

                        await _unitOfWork.GetRepository<PromoCode>().AddAsync(promoCode);

                        if (queue.Count > 1)
                        {
                            queue.Dequeue();
                        }
                    }

                    await transaction.CommitAsync();
                }
                catch
                {
                    await transaction.RollbackAsync();
                    throw;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.InnerException ?? ex, "{CONTROLLER_NAME} - Ошибка при выполнении метода", CONTROLLER_NAME);
                return BadRequest();
            }

            return Ok();
        }


        /// <summary>
        /// Получить все промо-коды
        /// </summary>
        /// <returns></returns>
        [HttpGet("getAll")]
        public async Task<ActionResult<List<PromoCodeShortModel>>> GetPromoCodesAllAsync()
        {
            var list = await _unitOfWork.GetRepository<PromoCode>().GetAllAsync(p => p.PartnerManager, p => p.Customer, p => p.Preference);

            return Ok(list.Select(promoCode => new PromoCodeModel()
            {
                Id = promoCode.Id,
                BeginDate = promoCode.BeginDate.ToString("dd.MM.yyyy HH:mm:ss zzz"),
                EndDate = promoCode.EndDate.ToString("dd.MM.yyyy HH:mm:ss zzz"),
                Code = promoCode.Code,
                PartnerName = promoCode.PartnerName,
                ServiceInfo = promoCode.ServiceInfo,
                Preference = promoCode.Preference == null ? null
                : new PreferenceModel
                {
                    Id = promoCode.PreferenceId,
                    Name = promoCode.Preference.Name
                },
                Customer = promoCode.Customer == null ? null
                : new CustomerShortModel
                {
                    Id = promoCode.Customer.Id,
                    Email = promoCode.Customer.Email,
                    FullName = promoCode.Customer.FullName
                },
                PartnerManager = promoCode.PartnerManager == null ? null
                : new EmployeeShortModel
                {
                    Id = promoCode.PartnerManager.Id,
                    Email = promoCode.PartnerManager.Email,
                    FullName = promoCode.PartnerManager.FullName,                    
                    AppliedPromocodesCount = promoCode.PartnerManager.AppliedPromocodesCount
                }
            }).ToList());
        }

        /// <summary>
        /// Получить промо-код по идентификатору
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<PromoCodeModel>> GetPromoCodeByIdAsync(Guid id)
        {
            var promoCode = await _unitOfWork.GetRepository<PromoCode>().GetByIdAsync(id, p => p.PartnerManager, p => p.Customer, p => p.Preference);
            if (promoCode == null) return NotFound();

            return new PromoCodeModel()
            {
                Id = promoCode.Id,
                BeginDate = promoCode.BeginDate.ToString("dd.MM.yyyy HH:mm:ss zzz"),
                EndDate = promoCode.EndDate.ToString("dd.MM.yyyy HH:mm:ss zzz"),
                Code = promoCode.Code,
                PartnerName = promoCode.PartnerName,
                ServiceInfo = promoCode.ServiceInfo, 
                Preference = promoCode.Preference == null ? null
                : new PreferenceModel
                {
                    Id = promoCode.PreferenceId,
                    Name = promoCode.Preference.Name
                },
                Customer = promoCode.Customer == null ? null
                : new CustomerShortModel
                {
                    Id = promoCode.Customer.Id,
                    Email = promoCode.Customer.Email,
                    FullName = promoCode.Customer.FullName
                },
                PartnerManager = promoCode.PartnerManager == null ? null
                : new EmployeeShortModel
                {
                    Id = promoCode.PartnerManager.Id,
                    Email = promoCode.PartnerManager.Email,
                    FullName = promoCode.PartnerManager.FullName,
                    AppliedPromocodesCount = promoCode.PartnerManager.AppliedPromocodesCount
                }
            };
        }

        /// <summary>
        /// Добавить промо-код
        /// </summary>
        /// <returns></returns>
        [HttpPost("add")]
        public async Task<ActionResult> AddAsync([FromBody] PromoCodeModel promoCode)
        {
            if (promoCode.Preference == null || promoCode.PartnerManager == null)
            {
                return BadRequest();
            }

            if (await _unitOfWork.GetRepository<PromoCode>().AnyAsync(promoCode.Id))
            {
                return Conflict();
            }

            try
            {
                await _unitOfWork.GetRepository<PromoCode>().AddAsync(new PromoCode
                {
                    Id = promoCode.Id,
                    BeginDate = DateTimeOffset.ParseExact(promoCode.BeginDate, "dd.MM.yyyy HH:mm:ss zzz", CultureInfo.InvariantCulture),
                    EndDate = DateTimeOffset.ParseExact(promoCode.EndDate, "dd.MM.yyyy HH:mm:ss zzz", CultureInfo.InvariantCulture),
                    Code = promoCode.Code,
                    PartnerName = promoCode.PartnerName,
                    ServiceInfo = promoCode.ServiceInfo,
                    PreferenceId = promoCode.Preference.Id,
                    PartnerManagerId = promoCode.PartnerManager.Id,
                    CustomerId = promoCode.Customer?.Id
                });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.InnerException ?? ex, "{CONTROLLER_NAME} - Ошибка при выполнении метода", CONTROLLER_NAME);
                return BadRequest();
            }

            return Ok();
        }

        /// <summary>
        /// Изменить промо-код
        /// </summary>
        /// <returns></returns>
        [HttpPut("update")]
        public async Task<ActionResult> UpdateAsync([FromBody] PromoCodeModel promoCode)
        {
            if (promoCode.Preference == null || promoCode.PartnerManager == null)
            {
                return BadRequest();
            }

            var existPromoCode = await _unitOfWork.GetRepository<PromoCode>().GetByIdAsync(promoCode.Id);
            if (existPromoCode == null) return NotFound();

            try
            {
                existPromoCode.BeginDate = DateTimeOffset.ParseExact(promoCode.BeginDate, "dd.MM.yyyy HH:mm:ss zzz", CultureInfo.InvariantCulture);
                existPromoCode.EndDate = DateTimeOffset.ParseExact(promoCode.EndDate, "dd.MM.yyyy HH:mm:ss zzz", CultureInfo.InvariantCulture);
                existPromoCode.Code = promoCode.Code;
                existPromoCode.PartnerName = promoCode.PartnerName;
                existPromoCode.ServiceInfo = promoCode.ServiceInfo;
                existPromoCode.PreferenceId = promoCode.Preference.Id;
                existPromoCode.PartnerManagerId = promoCode.PartnerManager.Id;
                existPromoCode.CustomerId = promoCode.Customer?.Id;

                await _unitOfWork.GetRepository<PromoCode>().UpdateAsync(existPromoCode);
            }
            catch
            {
                return BadRequest();
            }

            return Ok();
        }

        /// <summary>
        /// Удалить промо-код
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteAsync(Guid id)
        {
            if (!await _unitOfWork.GetRepository<PromoCode>().AnyAsync(id))
            {
                return NotFound();
            }

            try
            {
                await _unitOfWork.GetRepository<PromoCode>().RemoveAsync(id);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.InnerException ?? ex, "{CONTROLLER_NAME} - Ошибка при выполнении метода", CONTROLLER_NAME);
                return BadRequest();
            }

            return Ok();
        }
    }
}