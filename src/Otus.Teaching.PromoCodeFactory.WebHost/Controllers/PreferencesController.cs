﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController : ControllerBase
    {
        private const string CONTROLLER_NAME = nameof(PreferencesController);

        private ILogger<CustomersController> _logger;
        private readonly IRepository<Preference> _preferencesRepository;

        public PreferencesController(IRepository<Preference> preferencesRepository, ILogger<CustomersController> logger)
        {
            _preferencesRepository = preferencesRepository;
            _logger = logger;
        }

        /// <summary>
        /// Получить все доступные предпочтения
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<PreferenceModel>> GetPreferencesAsync()
        {
            var preferences = await _preferencesRepository.GetAllAsync();

            var preferencesModelList = preferences.Select(x =>
                new PreferenceModel()
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToList();

            return preferencesModelList;
        }

        /// <summary>
        /// Получить предпочтение по идентификатору
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<PreferenceModel>> GetPreferenceByIdAsync(Guid id)
        {
            var preference = await _preferencesRepository.GetByIdAsync(id);
            if (preference == null) return NotFound();

            return new PreferenceModel()
            {
                Id = preference.Id,
                Name = preference.Name
            };
        }

        /// <summary>
        /// Добавить предпочтение
        /// </summary>
        /// <returns></returns>
        [HttpPost("add")]
        public async Task<ActionResult> AddAsync([FromBody] PreferenceModel preference)
        {
            if (await _preferencesRepository.AnyAsync(preference.Id))
            {
                return Conflict();
            }

            try
            {
                await _preferencesRepository.AddAsync(new Preference
                {
                    Id = preference.Id,
                    Name = preference.Name
                });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.InnerException ?? ex, "{CONTROLLER_NAME} - Ошибка при выполнении метода", CONTROLLER_NAME);
                return BadRequest();
            }

            return Ok();
        }

        /// <summary>
        /// Изменить предпочтение
        /// </summary>
        /// <returns></returns>
        [HttpPut("update")]
        public async Task<ActionResult> UpdateAsync([FromBody] PreferenceModel preference)
        {
            var existPreference = await _preferencesRepository.GetByIdAsync(preference.Id);
            if (existPreference == null) return NotFound();

            try
            {
                existPreference.Name = preference.Name;

                await _preferencesRepository.UpdateAsync(existPreference);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.InnerException ?? ex, "{CONTROLLER_NAME} - Ошибка при выполнении метода", CONTROLLER_NAME);
                return BadRequest();
            }

            return Ok();
        }

        /// <summary>
        /// Удалить предпочтение
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteAsync(Guid id)
        {
            if (!await _preferencesRepository.AnyAsync(id))
            {
                return NotFound();
            }

            try
            {
                await _preferencesRepository.RemoveAsync(id);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.InnerException ?? ex, "{CONTROLLER_NAME} - Ошибка при выполнении метода", CONTROLLER_NAME);
                return BadRequest();
            }

            return Ok();
        }
    }
}
