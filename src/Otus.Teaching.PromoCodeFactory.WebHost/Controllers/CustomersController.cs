﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Managers;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Requests;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Responses;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private const string CONTROLLER_NAME = nameof(CustomersController);

        private ILogger<CustomersController> _logger;
        private readonly IUnitOfWork _unitOfWork;

        public CustomersController(IUnitOfWork unitOfWork, ILogger<CustomersController> logger)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
        }

        /// <summary>
        /// Получить список клиентов
        /// </summary>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        [HttpGet]
        public async Task<ActionResult<List<CustomerShortModel>>> GetCustomersAsync()
        {
            var customers = await _unitOfWork.GetRepository<Customer>().GetAllAsync();
            return Ok(customers.Select(x => new CustomerShortModel
            {
                Id = x.Id,
                Email = x.Email,
                FullName = x.FullName
            }).ToList());
        }

        /// <summary>
        /// Получить клиента вместе с выданными ему промо-кодами и предпочтениями
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _unitOfWork.GetRepository<Customer>().GetByIdAsync(id, c => c.PromoCodes);
            if (customer == null) return NotFound();

            var preferences = await _unitOfWork.GetRepository<CustomerPreference>().GetByFilterAsync(c => c.CustomerId == id, p => p.Preference);

            return Ok(new CustomerResponse
            {
                Id = customer.Id,
                Email = customer.Email,
                FullName = customer.FullName,
                Preferences = preferences.Select(x => new PreferenceModel
                {
                    Id = x.Preference.Id,
                    Name = x.Preference.Name
                }).ToList(),
                PromoCodes = customer.PromoCodes.Select(x => new PromoCodeShortModel
                {
                    Id = x.Id,
                    BeginDate = x.BeginDate.ToString("dd.MM.yyyy HH:mm:ss zzz"),
                    EndDate = x.EndDate.ToString("dd.MM.yyyy HH:mm:ss zzz"),
                    Code = x.Code,
                    PartnerName = x.PartnerName,
                    ServiceInfo = x.ServiceInfo
                }).ToList(),
            });
        }

        /// <summary>
        /// Добавить нового клиента вместе с его предпочтениями
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            try
            {
                var preferenceIds = request.PreferenceIds?.ToArray() ?? Array.Empty<Guid>();
                using var transaction = await _unitOfWork.BeginTransactionAsync();
                try
                {
                    var customerId = Guid.NewGuid();

                    await _unitOfWork.GetRepository<Customer>().AddAsync(new Customer
                    {
                        Id = customerId,
                        FirstName = request.FirstName,
                        LastName = request.LastName,
                        MiddleName = request.MiddleName,
                        Email = request.Email
                    });

                    if (preferenceIds.Any())
                    {
                        await _unitOfWork.GetRepository<CustomerPreference>().AddRangeAsync(preferenceIds
                            .Select(preferenceId => new CustomerPreference
                            {
                                Id = Guid.NewGuid(),
                                CustomerId = customerId,
                                PreferenceId = preferenceId
                            }));
                    }


                    await transaction.CommitAsync();
                }
                catch
                {
                    await transaction.RollbackAsync();
                    throw;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.InnerException ?? ex, "{CONTROLLER_NAME} - Ошибка при выполнении метода", CONTROLLER_NAME);
                return BadRequest();
            }

            return Ok();
        }

        /// <summary>
        /// Обновить данные клиента вместе с его предпочтениями
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> EditCustomersAsync([FromRoute] Guid id, [FromBody] CreateOrEditCustomerRequest request)
        {
            var customer = await _unitOfWork.GetRepository<Customer>().GetByIdAsync(id);
            if (customer == null) return NotFound();

            try
            {
                var preferenceIds = request.PreferenceIds?.ToArray() ?? Array.Empty<Guid>();
                using var transaction = await _unitOfWork.BeginTransactionAsync();
                try
                {
                    customer.Email = request.Email;
                    customer.FirstName = request.FirstName;
                    customer.LastName = request.LastName;
                    customer.MiddleName = request.MiddleName;

                    await _unitOfWork.GetRepository<Customer>().UpdateAsync(customer);

                    await _unitOfWork.GetRepository<CustomerPreference>().RemoveAsync(c => c.CustomerId == id);

                    if (preferenceIds.Any())
                    {
                        await _unitOfWork.GetRepository<CustomerPreference>().AddRangeAsync(preferenceIds
                            .Select(preferenceId => new CustomerPreference
                            {
                                Id = Guid.NewGuid(),
                                CustomerId = id,
                                PreferenceId = preferenceId
                            }));
                    }


                    await transaction.CommitAsync();
                }
                catch
                {
                    await transaction.RollbackAsync();
                    throw;
                }
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.InnerException ?? ex, "{CONTROLLER_NAME} - Ошибка при выполнении метода", CONTROLLER_NAME);
                return BadRequest();
            }

            return Ok();
        }

        /// <summary>
        /// Удалить клиента вместе с выданными ему промо-кодами
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            if (!await _unitOfWork.GetRepository<Customer>().AnyAsync(id))
            {
                return NotFound();
            }

            try
            {
                using var transaction = await _unitOfWork.BeginTransactionAsync();
                try
                {
                    await _unitOfWork.GetRepository<CustomerPreference>().RemoveAsync(c => c.CustomerId == id);
                    await _unitOfWork.GetRepository<PromoCode>().RemoveAsync(c => c.CustomerId == id);
                    await _unitOfWork.GetRepository<Customer>().RemoveAsync(id);

                    await transaction.CommitAsync();
                }
                catch
                {
                    await transaction.RollbackAsync();
                    throw;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.InnerException ?? ex, "{CONTROLLER_NAME} - Ошибка при выполнении метода", CONTROLLER_NAME);
                return BadRequest();
            }

            return Ok();
        }
    }
}