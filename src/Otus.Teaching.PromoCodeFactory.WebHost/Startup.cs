using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Otus.Teaching.PromoCodeFactory.Common.Options;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Managers;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Extensions;
using Otus.Teaching.PromoCodeFactory.DataAccess.Managers;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost
{
    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            var databaseSettings = _configuration.GetSection(DatabaseSettings.Position);

            services.AddOptions();
            services.Configure<DatabaseSettings>(databaseSettings);

            services.AddDbContext<DatabaseContext>(options =>
            {
                options.UseSqlite(databaseSettings.Get<DatabaseSettings>().ConnectionString);
            });

            services.AddTransient<DatabaseInitializer>();

            services.AddScoped<IRepository<Employee>, EfRepository<Employee>>();
            services.AddScoped<IRepository<Role>, EfRepository<Role>>();
            services.AddScoped<IRepository<Customer>, EfRepository<Customer>>();
            services.AddScoped<IRepository<CustomerPreference>, EfRepository<CustomerPreference>>();
            services.AddScoped<IRepository<Preference>, EfRepository<Preference>>();
            services.AddScoped<IRepository<PromoCode>, EfRepository<PromoCode>>();

            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddControllers();

            services.AddOpenApiDocument(options =>
            {
                options.Title = "PromoCode Factory API Doc";
                options.Version = "1.0";
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IHostApplicationLifetime lifetime)
        {
            lifetime.ApplicationStarted.Register(() => OnApplicationStartedAsync(app, env.IsProduction()).ConfigureAwait(false).GetAwaiter().GetResult());

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseOpenApi();
            app.UseSwaggerUi3(x =>
            {
                x.DocExpansion = "list";
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private async Task OnApplicationStartedAsync(IApplicationBuilder app, bool isProduct)
        {
            using var scope = app.ApplicationServices.CreateScope();
            var initializer = scope.ServiceProvider.GetRequiredService<DatabaseInitializer>();

            if (!isProduct)
            {
                var databaseSettings = scope.ServiceProvider.GetRequiredService<IOptions<DatabaseSettings>>()?.Value ?? new DatabaseSettings();
                await initializer.SetInitializeAsync(databaseSettings.RecreateDatabase, databaseSettings.InitializeFakeData);
            }
            else
            {
                await initializer.SetInitializeAsync(false, false);
            }
        }
            
    }
}