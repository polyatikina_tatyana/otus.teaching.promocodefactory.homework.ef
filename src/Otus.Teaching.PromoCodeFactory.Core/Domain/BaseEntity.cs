﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain
{
    public class BaseEntity
    {
        public BaseEntity() 
        {
            Created = DateTime.UtcNow;
            Modify = DateTime.UtcNow;
        }

        [Column("id")]
        public Guid Id { get; set; }

        [Column("created")]
        public DateTime Created { get; set; }

        [Column("modify")]
        public DateTime Modify { get; set; }
    }
}