﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    [Table("preference")]
    [Description("Предпочтения")]
    public partial class Preference
        : BaseEntity
    {
        public Preference()
        {
            CustomerPreferences = new HashSet<CustomerPreference>();
        }

        [MaxLength(255)]
        [Required(AllowEmptyStrings = false)]
        [Column("name")]
        public string Name { get; set; }

        public virtual ICollection<CustomerPreference> CustomerPreferences { get; set; }
        public virtual ICollection<PromoCode> PromoCodes { get; set; }
    }
}