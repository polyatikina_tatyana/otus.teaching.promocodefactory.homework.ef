﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    [Table("customer")]
    [Description("Клиенты")]
    public partial class Customer
        : BaseEntity
    {
        public Customer()
        {
            CustomerPreferences = new HashSet<CustomerPreference>();
            PromoCodes = new HashSet<PromoCode>();
        }

        [MaxLength(150)]
        [Required(AllowEmptyStrings = false)]
        [Column("first_name")]
        public string FirstName { get; set; }

        [MaxLength(150)]
        [Required(AllowEmptyStrings = false)]
        [Column("last_name")]
        public string LastName { get; set; }

        [MaxLength(150)]
        [Column("middle_name")]
        public string MiddleName { get; set; }

        [MaxLength(255)]
        [Column("email")]
        public string Email { get; set; }

        public virtual ICollection<CustomerPreference> CustomerPreferences { get; set; }

        public virtual ICollection<PromoCode> PromoCodes { get; set; }

        [IgnoreDataMember]
        public string FullName => $"{FirstName} {LastName} {MiddleName}".Trim();
    }
}