﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    [Table("promo_code")]
    [Description("Промо-коды")]
    public partial class PromoCode
        : BaseEntity
    {
        [MaxLength(150)]
        [Required(AllowEmptyStrings = false)]
        [Column("code")]
        public string Code { get; set; }

        [MaxLength(150)]
        [Required(AllowEmptyStrings = false)]
        [Column("service_info")]
        public string ServiceInfo { get; set; }

        [Column("begin_date")]
        public DateTimeOffset BeginDate { get; set; }

        [Column("end_date")]
        public DateTimeOffset EndDate { get; set; }

        [MaxLength(500)]
        [Required(AllowEmptyStrings = false)]
        [Column("partner_name")]
        public string PartnerName { get; set; }

        [Column("partner_manager_id")]
        public Guid PartnerManagerId { get; set; }

        [Column("preference_id")]
        public Guid PreferenceId { get; set; }

        [Column("customer_id")]
        public Guid? CustomerId { get; set; }
                
        public virtual Employee PartnerManager { get; set; }

        public virtual Preference Preference { get; set; }

        public virtual Customer Customer { get; set; }
    }
}