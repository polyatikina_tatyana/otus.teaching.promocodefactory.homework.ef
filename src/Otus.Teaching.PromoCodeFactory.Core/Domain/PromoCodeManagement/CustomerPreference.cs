﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    [Table("customer_preference")]
    [Description("Предпочтения клиента")]
    public partial class CustomerPreference : BaseEntity
    {
        [Column("preference_id")]
        public Guid PreferenceId { get; set; }

        [Column("customer_id")]
        public Guid CustomerId { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual Preference Preference { get; set; }
    }
}
