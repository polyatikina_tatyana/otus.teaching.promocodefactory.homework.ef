﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    [Table("employee")]
    [Description("Сотрудники")]
    public partial class Employee
        : BaseEntity
    {
        [MaxLength(150)]
        [Required(AllowEmptyStrings = false)]
        [Column("first_name")]
        public string FirstName { get; set; }

        [MaxLength(150)]
        [Required(AllowEmptyStrings = false)]
        [Column("last_name")]
        public string LastName { get; set; }

        [MaxLength(150)]
        [Column("middle_name")]
        public string MiddleName { get; set; }

        [MaxLength(255)]
        [Column("email")]
        public string Email { get; set; }

        [Column("role_id")]
        public Guid RoleId { get; set; }

        [Column("applied_promo_codes_count")]
        public int AppliedPromocodesCount { get; set; }

        public virtual Role Role { get; set; }

        public virtual ICollection<PromoCode> PromoCodes { get; set; }

        [IgnoreDataMember]
        public string FullName => $"{FirstName} {LastName} {MiddleName}".Trim();
    }
}