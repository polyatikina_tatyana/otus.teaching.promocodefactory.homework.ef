﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    [Table("role")]
    [Description("Роли")]
    public partial class Role
        : BaseEntity
    {
        public Role()
        {
            Employees = new HashSet<Employee>();
        }

        [MaxLength(255)]
        [Required(AllowEmptyStrings = false)]
        [Column("name")]
        public string Name { get; set; }

        [MaxLength(500)]
        [Column("description")]
        public string Description { get; set; }

        public virtual ICollection<Employee> Employees { get; set; }
    }
}