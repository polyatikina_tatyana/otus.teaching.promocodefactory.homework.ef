﻿namespace Otus.Teaching.PromoCodeFactory.Common.Options
{
    public class DatabaseSettings
    {
        public const string Position = nameof(DatabaseSettings);

        public string ConnectionString { get; set; }
        public bool RecreateDatabase { get; set; }
        public bool InitializeFakeData { get; set; }
    }
}
